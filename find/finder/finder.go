package finder

import (
	"bytes"
	"context"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"sync"
)

// Finder is an object, that can search files in a file tree.
type Finder struct {
	path  string
	level int
}

// New returns a new Finder,
// that searches files in a file tree starting from path.
func New(path string) *Finder {
	return NewLevel(path, -1)
}

// NewLevel returns a new Finder,
// that searched files in a file tree starting from path not deeper than level.
func NewLevel(path string, level int) *Finder {
	return &Finder{path: path, level: level}
}

type finderJob struct {
	filename string
	offset   int64
	size     int64
	found    *bool
	mutex    *sync.Mutex
}

var dumps int

// FindIn searches an occurence of subbbytes in b and
// reports whether it's found after the search and whether it was found before.
func (job finderJob) findIn(b, subbytes []byte) (found, wasFound bool) {
	if job.mutex != nil {
		job.mutex.Lock()
		defer job.mutex.Unlock()
	}
	wasFound = *job.found
	if !wasFound {
		found = bytes.Contains(b, subbytes)
	}
	*job.found = found || wasFound
	return found, wasFound
}

// Find searces an occurence of subbbytes in file section described by job
func (job finderJob) find(subbytes []byte) (found bool, err error) {

	dn := len(subbytes)

	file, err := os.Open(job.filename)
	if err != nil {
		return false, err
	}
	defer file.Close()

	// The same file could also be found from another routine during the search.
	// In this case we should stop searching.
	// This var reports whether this case has happened.
	var foundByOtherRoutine bool

	// Chosing a size which is multiple to the default buffer size.
	const defaultBufSize = 4096
	bufSize := ((dn / defaultBufSize) + 1) * defaultBufSize

	b := make([]byte, bufSize)
	fileSection := io.NewSectionReader(file, job.offset, job.size)

	// Offset to the start of reading the file section.
	var off int
	for {
		n, err := fileSection.Read(b[off:])
		if err != nil && err != io.EOF {
			return false, err
		}
		found, foundByOtherRoutine = job.findIn(b[:off+n], subbytes)
		if found || foundByOtherRoutine || err == io.EOF {
			break
		}
		// The last bytes that was read limited by the length of subbbytes.
		var lastbytes []byte
		if dn < n {
			lastbytes = b[n-dn : n]
		} else {
			lastbytes = b[:n]
		}
		// Copying them to the start of b
		// to handle an occurence on an edge of the last two Read()'s.
		off = copy(b, lastbytes)
	}

	return found, nil
}

// Hyperparameters to configure parallel search.
const (
	RoutinesPerCPU     = 2
	FileSizePerRoutine = 1 << 20
)

// BySubstringAsync searches for a file that contains s
// and calls cb for each found file with name. Returns done channel and err, if happened.
func (f *Finder) BySubstringAsync(s string, cb func(name string)) error {

	// Creating a slice of bytes from string to find.
	b := []byte(s)
	nbytes := int64(len(b))

	// Creating a channel which represents a queue for free routines,
	// where int - is an index of the first free routine in a queue.
	freeRoutines := make(chan int)

	// Creating a channel which represents a queue of jobs for routines.
	jobs := make([]chan finderJob, RoutinesPerCPU*runtime.NumCPU())

	// Creating a context for routines to release resources, when all files will be found.
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// A error that can happen while doing a job.
	var jobErr error

	for routine := range jobs {

		job := make(chan finderJob)

		// Starting up routines that will search substrings
		// at different file sections of a file tree.
		go func(routine int, job chan finderJob) {

			// Making a copy of b on go routine stack to speed up an access to it.
			subbytes := make([]byte, nbytes)
			copy(subbytes, b)

			freeRoutines <- routine

			// Handling jobs from the queue...
			for job := range job {

				found, err := job.find(subbytes)

				if found {
					cb(job.filename)
				} else if err != nil {
					cancel()
					jobErr = err
				}

				select {
				// Standing in the queue of free go routines...
				case freeRoutines <- routine:
					continue

				// But if error happened in other jobs, then ending here.
				case <-ctx.Done():
					return
				}
			}
		}(routine, job)

		jobs[routine] = job
	}

	// Closing all jobs to finish all go routines.
	defer func() {
		for _ = range jobs {
			<-freeRoutines
		}
		for _, job := range jobs {
			close(job)
		}
	}()

	// Walking through all files starting from path and downside the file tree...
	err := filepath.Walk(f.path, func(path string, file os.FileInfo, err error) error {

		if err != nil {
			// If error happened, then ending here..
			return err
		}
		if file.IsDir() {
			if f.level < 0 {
				return nil
			}
			// Checking the restriction on level of recursion.
			for level := 0; level < f.level; level++ {
				if path == f.path {
					return nil
				}
				path = filepath.Dir(path)
			}
			return filepath.SkipDir
		}

		found := new(bool)

		var mutex *sync.Mutex
		if file.Size() > FileSizePerRoutine {
			mutex = new(sync.Mutex)
		}

		var offset, size, ds int64
		for offset = 0; offset < file.Size(); offset += size {

			// Creating a job that finds an occurence of substring s
			// from in range of file bytes [offset, offset + size + ds).
			if offset+FileSizePerRoutine > file.Size() {
				size, ds = file.Size(), 0
			} else {
				size, ds = FileSizePerRoutine, int64(len(b))
			}
			// Extra size ds to handle an occurence on an edge of two file sections.
			job := finderJob{path, offset, size + ds, found, mutex}

			select {
			// Trying to find a free go routine to do this job.
			case freeRoutine := <-freeRoutines:
				jobs[freeRoutine] <- job

			// But if error happened in jobs, then ending here.
			case <-ctx.Done():
				return jobErr
			}
		}
		return nil
	})
	if err != nil {
		// If a error has allready happened, then returning it.
		return err
	}
	// Otherwise a error that could happened in job should be returned.
	return jobErr
}
