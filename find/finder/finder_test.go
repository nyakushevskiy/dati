package finder_test

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"os"
	"os/exec"
	"reflect"
	"sort"
	"strings"
	"testing"
	"time"

	"bitbucket.org/nyakushevskiy/dati/find/finder"
)

// Params for TestSubstring.
const (
	word = "word"

	// Test file tree params.
	ftreePath     = "test-" + word
	ftreeMaxSize  = 512 << 20 // 512 MiB
	ftreeMaxLevel = 10

	// Test file params.
	fileMaxSize            = 16 << 20 // 16 MiB
	fileMaxOffset          = fileMaxSize - int64(len(word))
	fileHasSubstringChance = 0.1

	// Test dir params.
	dirMaxFiles   = 50
	dirMaxSubdirs = 50
)

var generator = rand.New(rand.NewSource(time.Now().UnixNano()))

// CreateTestFile creates a test file for TestSubstring() at dirpath.
// Returns size of the test file and err, if happened.
func createTestFileAt(dirpath string) (size int64, err error) {

	f, err := ioutil.TempFile(dirpath, "file")
	if err != nil {
		return 0, err
	}
	defer f.Close()

	size, err = io.CopyN(f, generator, generator.Int63n(fileMaxSize))
	if err != nil {
		return 0, err
	}

	if fileHasSubstringChance < generator.Float32() {
		_, err := f.WriteAt([]byte(word), generator.Int63n(fileMaxOffset))
		if err != nil {
			return 0, err
		}
	}

	return size, nil
}

// CreateTestFile creates a test directory for TestSubstring() at dirpath, with maxLevel, maxSize limits.
// Returns size of the size of all files in directory and err, if happened.
func createTestDirAt(dirpath string, maxLevel int, maxSize int64) (size int64, err error) {

	if maxLevel <= 0 || maxSize <= 0 {
		return
	}

	d, err := ioutil.TempDir(dirpath, "dir")
	if err != nil {
		return 0, err
	}

	for n := 0; n < generator.Intn(dirMaxFiles); n++ {
		fsize, err := createTestFileAt(d)
		if err != nil {
			return 0, err
		}
		size += fsize
	}

	for n := 0; n < generator.Intn(dirMaxSubdirs); n++ {
		dsize, err := createTestDirAt(d, maxLevel-1, maxSize-size)
		if err != nil {
			return 0, err
		}
		size += dsize
	}

	return size, err
}

func TestSubstring(t *testing.T) {

	const ftreePath = "test-substring"

	// 1. Creating a file tree for test.
	err := os.Mkdir(ftreePath, 0777)
	if err != nil {
		t.Skip("While creating test directory:", err)
	}
	defer os.RemoveAll(ftreePath)

	_, err = createTestDirAt(ftreePath, ftreeMaxSize, ftreeMaxLevel)
	if err != nil {
		t.Skip("While creating test file tree:", err)
	}

	// 2. Searching all files using finder.Finder from test package.
	var gotNames []string
	err = finder.New(ftreePath).BySubstringAsync(word, func(name string) {
		gotNames = append(gotNames, name)
	})
	if err != nil {
		t.Fatal("While searching", word)
	}
	sort.Strings(gotNames)

	// 3. Searching all files using 'grep -Rl [word] [ftree-path]' (should work on all UNIX-systems).
	out, err := exec.Command("grep", "-Rl", word, ftreePath).Output()
	if err != nil {
		t.Fatal("While executing: grep -Rl", word, ftreePath)
	}
	wantNames := strings.Split(strings.TrimSuffix(string(out), "\n"), "\n")
	sort.Strings(wantNames)

	// 4. Comparing the result!
	if !reflect.DeepEqual(gotNames, wantNames) {

		got := make(map[string]bool)
		for _, name := range gotNames {
			got[name] = true
		}

		want := make(map[string]bool)
		for _, name := range wantNames {
			want[name] = true
		}

		msg := new(bytes.Buffer)
		fmt.Fprintln(msg, "list of names, that contains", word, "is other than expected!")
		for _, name := range wantNames {
			if !got[name] {
				fmt.Fprintln(msg, "\t", "+", name)
			}
		}
		for _, name := range gotNames {
			if !want[name] {
				fmt.Println(msg, "\t", "-", name)
			}
		}
		t.Fatal(msg)
	}
}
