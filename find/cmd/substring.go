package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// SubstringCmd represents the substring command.
var substringCmd = &cobra.Command{
	Use:   "substring",
	Short: "Searches and asynchroniously prints names of files that contains a substring.",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		find.BySubstringAsync(args[0], func(name string) { fmt.Println(name) })
	},
}

func init() {
	rootCmd.AddCommand(substringCmd)
}
