package cmd

import (
	"os"

	"github.com/spf13/cobra"

	"bitbucket.org/nyakushevskiy/dati/find/finder"
)

var find *finder.Finder

// RootCmd represents the base command when called without any subcommands.
var rootCmd = &cobra.Command{
	Use:   "find",
	Short: "The tool for recursive search of files in a file tree.",
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		if _, err := os.Stat(path); err == os.ErrNotExist {
			return err
		}
		find = finder.NewLevel(path, level)
		return nil
	},
}

var path string
var level int

func init() {
	rootCmd.PersistentFlags().StringVarP(&path, "path", "p", ".", "Path to file where a file tree starts")
	rootCmd.PersistentFlags().IntVarP(&level, "level", "l", -1, "Max level of recursion while walking through a file tree")
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
